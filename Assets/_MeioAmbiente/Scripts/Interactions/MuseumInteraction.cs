﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum MuseumQuestionType
{
    A, O
}

[System.Serializable]
public struct MuseumPainting
{
    public string Name;
    public Sprite Image;
    public string Author;

    public MuseumPainting(string name, string author, Sprite image)
    {
        Name = name;
        Author = author;
        Image = image;
    }
}

[System.Serializable]
public struct MuseumQuestion
{
    public string Question;
    public string Answer;
    public MuseumQuestionType Type;

    public MuseumQuestion(string question, string answer, MuseumQuestionType type)
    {
        Question = question;
        Answer = answer;
        Type = type;
    }
}

public class MuseumInteraction : Interaction {

    [Header("Data")]
    public TextAsset PaintingsTSV;
    public TextAsset QuestionsTSV;

    public List<MuseumPainting> Paintings;
    public List<MuseumQuestion> Questions;

    [Header("UI")]
    public TextMeshProUGUI Text;
    public MuseumButton ButtonPrefab;
    public List<MuseumButton> Buttons;

    public QuestionInteraction QuestionController;

    private int _questionIndex;

    public MuseumQuestion CurrentQuestion
    {
        get
        {
            return Questions[_questionIndex];
        }
    }

    private DraggableElement _drag;
    public DraggableElement Drag
    {
        get
        {
            if (_drag == null)
                _drag = GetComponent<DraggableElement>();
            return _drag;
        }
    }

    private void Awake()
    {
        /*ReadPaintingsTSV();
        ReadQuestionsTSV();

        SetButtons();*/
    }

    private void OnEnable()
    {
        if (Paintings == null || Paintings.Count == 0)
            ReadPaintingsTSV();
        if (Questions == null || Questions.Count == 0)
            ReadQuestionsTSV();

        _questionIndex = Random.Range(0, Questions.Count);

        SetButtons();
    }

    public void Answer(MuseumButton button)
    {
        print(button.Painting.Author + " " + button.Painting.Name);

        Text.gameObject.SetActive(false);
        QuestionController.Balloon.GetComponent<Graphic>().enabled = true;
        QuestionController.Question.GetComponent<Graphic>().enabled = true;
        QuestionController.CurrentNPC.GetComponent<Graphic>().enabled = true;
        QuestionController.CurrentNPC.MouthGraphic.GetComponent<Graphic>().enabled = true;

        switch (CurrentQuestion.Type)
        {
            case MuseumQuestionType.A:
                if (button.Painting.Author == CurrentQuestion.Answer)
                    OnFinish.Invoke(true);
                else
                    OnFinish.Invoke(false);
                break;
            case MuseumQuestionType.O:
                if (button.Painting.Name == CurrentQuestion.Answer)
                    OnFinish.Invoke(true);
                else
                    OnFinish.Invoke(false);
                break;
            default:
                break;
        }
        gameObject.SetActive(false);
    }

    private void ReadPaintingsTSV()
    {
        Paintings = new List<MuseumPainting>();

        string[] lines = PaintingsTSV.text.Split(System.Environment.NewLine[0]);

        for (int i = 1; i < lines.Length; i++)
        {
            string[] contents = lines[i].Split("\t"[0]);
            
            string name = contents[0].Trim();

            string author = contents[1].Trim();

            Sprite image = Resources.Load<Sprite>("Museu/" + contents[2].Trim());

            Paintings.Add(new MuseumPainting(name, author, image));
        }
    }

    private void ReadQuestionsTSV()
    {
        Questions = new List<MuseumQuestion>();

        string[] spaceLines = QuestionsTSV.text.Split(System.Environment.NewLine[0]);

        for (int i = 1; i < spaceLines.Length; i++)
        {
            string[] contents = spaceLines[i].Split("\t"[0]);

            MuseumQuestionType type = (MuseumQuestionType)System.Enum.Parse(typeof(MuseumQuestionType), contents[0].Trim().ToUpper());

            string question = contents[1].Trim();

            string answer = contents[2].Trim();

            Questions.Add(new MuseumQuestion(question, answer, type));
        }
    }

    private void SetButtons()
    {
        //QuestionController.Question.text = CurrentQuestion.Question;
        Text.gameObject.SetActive(true);
        Text.text = CurrentQuestion.Question;
        print(CurrentQuestion.Question);

        QuestionController.Balloon.GetComponent<Graphic>().enabled = false;
        QuestionController.Question.GetComponent<Graphic>().enabled = false;
        QuestionController.CurrentNPC.GetComponent<Graphic>().enabled = false;
        QuestionController.CurrentNPC.MouthGraphic.GetComponent<Graphic>().enabled = false;

        for (int i = 0; i < Buttons.Count; i++)
            Destroy(Buttons[i].gameObject);

        ButtonPrefab.gameObject.SetActive(true);
        ButtonPrefab.BorderImage.color = Color.white;
        ButtonPrefab.PaintingImage.color = Color.white;
        Buttons = new List<MuseumButton>();

        List<MuseumPainting> paintings = new List<MuseumPainting>(Paintings);

        for (int i = 0; i < paintings.Count; i++)
        {
            if ((CurrentQuestion.Type == MuseumQuestionType.A && paintings[i].Author == CurrentQuestion.Answer) || 
                (CurrentQuestion.Type == MuseumQuestionType.O && paintings[i].Name == CurrentQuestion.Answer))
            {
                MuseumButton button = Instantiate(ButtonPrefab);
                button.transform.SetParent(transform);
                button.Painting = paintings[i];
                button.OnClick.AddListener(Answer);
                Buttons.Add(button);

                paintings.RemoveAt(i);
                break;
            }
        }

        paintings.Shuffle();

        for (int i = 0; i < 19; i++)
        {
            MuseumButton button = Instantiate(ButtonPrefab);
            button.transform.SetParent(transform);
            button.Painting = paintings[i];
            button.OnClick.AddListener(Answer);
            Buttons.Add(button);
        }

        Buttons.Shuffle();

        for (int i = 0; i < Buttons.Count; i++)
        {
            Vector2 position = Buttons[i].GetComponent<RectTransform>().anchoredPosition;
            position.x += 610 * i;
            position.y = -0;
            Buttons[i].GetComponent<RectTransform>().anchoredPosition = position;
        }

        ButtonPrefab.gameObject.SetActive(false);
    }
}

public static class Extention
{
    private static System.Random rng = new System.Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
